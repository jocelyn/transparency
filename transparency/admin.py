from django.contrib.admin import AdminSite
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model

User = get_user_model()

from costs.admin import CostAdmin, DocumentAdmin, GoodAdmin, ServiceAdmin
from costs.models import Cost, Document, Good, Service


class MyAdminSite(AdminSite):
    site_title = 'Administration de Transparency'
    site_header = site_title

    # Some models available for new/change but not listed in index pages.
    hidden_models = ['service', 'cost', 'good']

    def each_context(self, request):
        out = super().each_context(request)
        out['hidden_models'] = self.hidden_models
        return out


admin_site = MyAdminSite()
admin_site.register(User, UserAdmin)

admin_site.register(Cost, CostAdmin)
admin_site.register(Document, DocumentAdmin)
admin_site.register(Good, GoodAdmin)
admin_site.register(Service, ServiceAdmin)
