from django.conf import settings as settings_vars


def settings(request):
    return {'settings': {
        'ORGANIZATION_NAME': settings_vars.ORGANIZATION_NAME,
    }}
