from django.apps import AppConfig


class CostsConfig(AppConfig):
    name = 'costs'
    verbose_name = 'Coûts de revient'
