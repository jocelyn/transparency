from django.core.exceptions import ValidationError


def less_than_one(value):
    if value > 1:
        raise ValidationError('Should be less than {}'.format(value))
    return value
