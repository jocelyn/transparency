import unittest

from ..templatetags.costs import human_round


class TestFilters(unittest.TestCase):
    def test_human_round(self):

        results = (
            (0, '0'),
            (0.1, '0.1'),
            (0.12, '0.12'),
            (1, '1'),
            (1.12344589, '1.1'),
            (1.19, '1.2'),
            (0.00123, '0.0012'),
            (0.00000000001, '0'),
            (123, '123'),
            (12.1, '12'),
            )

        for inp, outp in results:
            self.assertEqual(human_round(inp), outp)
