import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import Cost, Document, Good, Service, GoodUse, CostUse


User = get_user_model()


class ViewsTests(TestCase):
    def setUp(self):
        user = User(username='user', is_staff=True, is_active=True)
        user.set_password('password')
        user.save()

        self.doc = Document.objects.create(name='budget')
        rent = Cost.objects.create(name='Rent', price=100, document=self.doc)
        server = Good.objects.create(
            name='Server',
            price=1000,
            provisioning_duration=datetime.timedelta(days=36*20.6),
            document=self.doc,
        )
        vpn = Service.objects.create(name='VPN', document=self.doc)
        GoodUse.objects.create(resource=server, service=vpn, share=0.5)
        CostUse.objects.create(resource=rent, service=vpn, share=0.5)

    def test_detail_service(self):
        assert self.client.login(username='user', password='password')
        response = self.client.get('/costs/services/1')
        self.assertEqual(response.status_code, 200)
