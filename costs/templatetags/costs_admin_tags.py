from django import template

register = template.Library()

hidden_models = {
    'costs': ('Service', 'Cost', 'Good')
}


@register.filter
def is_hidden(model, app):
    return (
        (app['app_label'] in hidden_models) and
        (model['object_name'] in hidden_models[app['app_label']]))
