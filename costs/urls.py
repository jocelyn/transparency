from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^documents/(?P<pk>\d+)/compare/(?P<other_pk>\d+)$', views.compare_document, name='compare-document'),
    url(r'^documents/(?P<pk>\d+)',
        views.detail_document,
        name='detail-document'),
    url(r'^documents$',
        views.list_documents,
        name='list-documents'),
    url(r'^services/(?P<pk>\d+)$', views.detail_service, name='detail-service'),
]
