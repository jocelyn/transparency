# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0009_auto_20160131_1208'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='reusable',
            field=models.BooleanField(help_text="Peut-être utilisé par d'autres services", default=False, verbose_name='Ré-utilisable'),
        ),
    ]
