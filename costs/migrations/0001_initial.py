# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import costs.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=130)),
                ('description', models.TextField(blank=True)),
                ('price', models.PositiveIntegerField(help_text='Coût mensuel')),
            ],
            options={
                'verbose_name': 'Coût',
            },
        ),
        migrations.CreateModel(
            name='CostUse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('share', models.FloatField(validators=[costs.validators.less_than_one])),
                ('resource', models.ForeignKey(to='costs.Cost')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Good',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=130)),
                ('description', models.TextField(blank=True)),
                ('price', models.PositiveIntegerField(help_text="Prix d'achat")),
                ('provisioning_duration', models.DurationField(choices=[(datetime.timedelta(1095), '3 ans'), (datetime.timedelta(1825), '5 ans')])),
            ],
            options={
                'verbose_name': 'Bien',
            },
        ),
        migrations.CreateModel(
            name='GoodUse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('share', models.FloatField(validators=[costs.validators.less_than_one])),
                ('resource', models.ForeignKey(to='costs.Good')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=130)),
                ('description', models.TextField(blank=True)),
                ('costs', models.ManyToManyField(through='costs.CostUse', related_name='using_services', to='costs.Cost')),
                ('goods', models.ManyToManyField(through='costs.GoodUse', related_name='using_services', to='costs.Good')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='gooduse',
            name='service',
            field=models.ForeignKey(to='costs.Service'),
        ),
        migrations.AddField(
            model_name='costuse',
            name='service',
            field=models.ForeignKey(to='costs.Service'),
        ),
    ]
