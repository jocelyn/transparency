# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0004_auto_20151129_2340'),
    ]

    operations = [
        migrations.AddField(
            model_name='cost',
            name='document',
            field=models.ForeignKey(to='costs.Document', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='good',
            name='document',
            field=models.ForeignKey(to='costs.Document', default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='document',
            name='type',
            field=models.CharField(choices=[('fact', 'relevé'), ('plan', 'scénario/estimation')], max_length=10),
        ),
    ]
