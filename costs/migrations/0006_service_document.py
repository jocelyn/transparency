# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0005_auto_20151129_2354'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='document',
            field=models.ForeignKey(to='costs.Document', default=1),
            preserve_default=False,
        ),
    ]
