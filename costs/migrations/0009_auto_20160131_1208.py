# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0008_auto_20160108_2331'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceUse',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('share', models.FloatField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='service',
            name='capacity_unit',
            field=models.CharField(blank=True, choices=[('a', 'A'), ('mbps', 'Mbps'), ('u', 'U'), ('ipv4', 'IPv4'), ('eth', 'ports'), ('services', 'services')], max_length=10),
        ),
        migrations.AddField(
            model_name='service',
            name='total_capacity',
            field=models.FloatField(default=1),
        ),
        migrations.AlterField(
            model_name='cost',
            name='capacity_unit',
            field=models.CharField(blank=True, choices=[('a', 'A'), ('mbps', 'Mbps'), ('u', 'U'), ('ipv4', 'IPv4'), ('eth', 'ports'), ('services', 'services')], max_length=10),
        ),
        migrations.AlterField(
            model_name='document',
            name='date',
            field=models.DateField(default=datetime.datetime.now),
        ),
        migrations.AlterField(
            model_name='good',
            name='capacity_unit',
            field=models.CharField(blank=True, choices=[('a', 'A'), ('mbps', 'Mbps'), ('u', 'U'), ('ipv4', 'IPv4'), ('eth', 'ports'), ('services', 'services')], max_length=10),
        ),
        migrations.AddField(
            model_name='serviceuse',
            name='resource',
            field=models.ForeignKey(to='costs.Service', related_name='dependent_services'),
        ),
        migrations.AddField(
            model_name='serviceuse',
            name='service',
            field=models.ForeignKey(to='costs.Service'),
        ),
    ]
