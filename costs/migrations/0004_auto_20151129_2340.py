# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0003_auto_20151128_1044'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=130)),
                ('comment', models.TextField(blank=True)),
                ('date', models.DateField(auto_now_add=True)),
                ('type', models.CharField(choices=[('fact', 'relevé'), ('plan', 'scénario/estimation')], max_length='10')),
            ],
        ),
        migrations.AlterField(
            model_name='service',
            name='subscriptions_count',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
