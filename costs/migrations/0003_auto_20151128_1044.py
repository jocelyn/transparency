# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0002_auto_20151128_1015'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cost',
            name='price',
            field=models.FloatField(help_text='Coût mensuel'),
        ),
        migrations.AlterField(
            model_name='good',
            name='price',
            field=models.FloatField(),
        ),
    ]
