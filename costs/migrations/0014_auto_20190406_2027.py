# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2019-04-06 18:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0013_auto_20161107_1333'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='document',
            options={'ordering': ['-date'], 'verbose_name': 'Rapport'},
        ),
        migrations.AddField(
            model_name='costuse',
            name='help',
            field=models.TextField(blank=True, help_text='Texte brut ou markdown', verbose_name='Aide'),
        ),
        migrations.AddField(
            model_name='gooduse',
            name='help',
            field=models.TextField(blank=True, help_text='Texte brut ou markdown', verbose_name='Aide'),
        ),
        migrations.AddField(
            model_name='serviceuse',
            name='help',
            field=models.TextField(blank=True, help_text='Texte brut ou markdown', verbose_name='Aide'),
        ),
        migrations.AlterField(
            model_name='document',
            name='type',
            field=models.CharField(choices=[('fact', 'Rapport public'), ('plan', 'Rapport brouillon')], help_text="Un rapport brouillon n'est pas visible publiquement", max_length=10),
        ),
        migrations.AlterField(
            model_name='service',
            name='internal',
            field=models.BooleanField(default=False, help_text="Ne peut être vendu tel quel, n'apparaît pas dans la liste des services", verbose_name='Service interne'),
        ),
        migrations.AlterField(
            model_name='service',
            name='reusable',
            field=models.BooleanField(default=False, help_text="Peut-être utilisé par d'autres services", verbose_name="Ré-utilisable par d'autres services"),
        ),
    ]
