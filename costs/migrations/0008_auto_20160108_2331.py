# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0007_auto_20151209_0027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cost',
            name='capacity_unit',
            field=models.CharField(choices=[('a', 'A'), ('mbps', 'Mbps'), ('u', 'U'), ('ipv4', 'IPv4'), ('eth', 'ports')], max_length=10, blank=True),
        ),
        migrations.AlterField(
            model_name='costuse',
            name='share',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='good',
            name='capacity_unit',
            field=models.CharField(choices=[('a', 'A'), ('mbps', 'Mbps'), ('u', 'U'), ('ipv4', 'IPv4'), ('eth', 'ports')], max_length=10, blank=True),
        ),
        migrations.AlterField(
            model_name='gooduse',
            name='share',
            field=models.FloatField(),
        ),
    ]
