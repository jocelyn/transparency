# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costs', '0006_service_document'),
    ]

    operations = [
        migrations.AddField(
            model_name='cost',
            name='capacity_unit',
            field=models.CharField(blank=True, max_length='10', choices=[('w', 'Watt'), ('mbps', 'Mbps'), ('u', 'U'), ('ipv4', 'IPv4')]),
        ),
        migrations.AddField(
            model_name='cost',
            name='total_capacity',
            field=models.FloatField(default=1),
        ),
        migrations.AddField(
            model_name='good',
            name='capacity_unit',
            field=models.CharField(blank=True, max_length='10', choices=[('w', 'Watt'), ('mbps', 'Mbps'), ('u', 'U'), ('ipv4', 'IPv4')]),
        ),
        migrations.AddField(
            model_name='good',
            name='total_capacity',
            field=models.FloatField(default=1),
        ),
    ]
