function edit_help_event(event) {
    var tr = django.jQuery(event.target).closest('tr');
    var view_help = tr.find('.field-view_help');
    view_help.hide();
    var help = tr.find('.field-help');
    help.show();
}

django.jQuery().ready(function() {
    // Hide all help field
    django.jQuery(".field-help").hide();

    // Hide header of help field
    django.jQuery("body").find(".field-view_help").closest("table").find("th:nth-child(3)").hide();

    // Add handler on edit button
    django.jQuery(".edit_help").click(edit_help_event);
});

