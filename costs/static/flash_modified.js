/** JS hack when the previous page is a save of a child service
 *
 * As we are redirected here after save of child service, we want to scroll
 * near that child inline and keep the top messages visible (thus moving
 * them near to the inline instead of page top).
 */

django.jQuery().ready(function() {
    var urlParams = new URLSearchParams(window.location.search);

    let modified_child_type = urlParams.get('modified_child_type');
    let saved_child_id = urlParams.get('saved_child_id');
    let messages = django.jQuery('.messagelist').detach();

    if (modified_child_type === 'service' && (messages.length > 0)) {
        messages.insertBefore(django.jQuery('#service_set-group'));
        messages[0].scrollIntoView();
    }
});
