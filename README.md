Transparency: detail costs for libre price
==========================================

It's still alpha-pre-ugly-looking and some strings are mentioning
[FAImaison](https://faimaison.net), more usable version
may be integrated to [coin](https://code.ffdn.org/ffdn/coin).

Install it
----------
Be sure to use python3.

    sudo apt-get install python virtualenv
    virtualenv transparency_venv
    git clone https://code.ffdn.org/jocelyn/transparency
    cd transparency
    source ../transparency_venv/bin/activate
    pip install -r requirements.txt

Generate a secret key:

    echo SECRET_KEY=`python -c "import string,random; uni=string.ascii_letters+string.digits+string.punctuation; print(repr(''.join([random.SystemRandom().choice(uni) for i in range(random.randint(45,50))])))"` > transparency/local_settings.py

Set your organization name :

    echo 'ORGANIZATION_NAME="ACME Charity"' >> transparency/local_settings.py

Create database:

    ./manage.py migrate

Create administrator account:

    ./manage.py createsuperuser

Run it
------

Run development server

    source ../transparency_venv/bin/activate # if it is not already done
    echo 'DEBUG=True' >> transparency/local_settings.py
    ./manage.py runserver

Use it
------

- Enter data into (yeah, documentation can explain a bit better...)
  http://localhost:8000/admin/
- Browse http://localhost:8000 to view costs/details
