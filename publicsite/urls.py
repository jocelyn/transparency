from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',
        views.public_document_detail,
        name='public-index'),
    url(r'^report/(?P<pk>\d+)$',
        views.public_document_detail,
        name='public-detail-document'),
]
