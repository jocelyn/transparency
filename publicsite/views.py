from django.shortcuts import get_object_or_404, render
from costs.models import Document


def public_document_detail(request, pk=None):
    docs = Document.objects\
                   .filter(type=Document.TYPE_PUBLIC)\
                   .order_by('-date')

    if pk is None:
        doc = docs.order_by('-date').first()
    else:
        doc = get_object_or_404(Document, pk=pk)

    return render(request, 'publicsite/public_document_detail.html', {
        'document': doc,
        'documents': docs,
    })
